{
  osConfig,
  lib,
  pkgs,
  ...
}:
let
  scaleArray =
    array: lib.concatStringsSep ", " (map (x: toString (x * osConfig.monitor_scale)) array);
  lock_cmd = "pidof ${pkgs.hyprlock}/bin/hyprlock || { ${pkgs.hyprlock}/bin/hyprlock && ${pkgs.eww}/bin/eww open bar; }"; # avoid starting multiple hyprlock instances.
in
{
  wayland.windowManager.hyprland.settings.bind = [ "$mainMod, L, exec, ${lock_cmd}" ];
  services.hypridle = {
    enable = true;
    settings = {
      general = {
        lock_cmd = lock_cmd;
        before_sleep_cmd = "loginctl lock-session"; # lock before suspend.
        after_sleep_cmd = "hyprctl dispatch dpms on"; # to avoid having to press a key twice to turn on the display.
      };

      listener = [

        {
          timeout = 300; # 5min
          on-timeout = "loginctl lock-session"; # lock screen when timeout has passed
        }
        {
          timeout = 330; # 5.5min
          on-timeout = "hyprctl dispatch dpms off"; # screen off when timeout has passed
          on-resume = "hyprctl dispatch dpms on"; # screen on when activity is detected after timeout has fired.
        }
      ];
    };
  };
  programs.hyprlock = {
    enable = true;
    settings = {
      general = {
        # pam_module = "login";
        disable_loading_bar = false;
      };

      background = {
        monitor = "";
        path = osConfig.wallpapers.lock;
        blur_passes = 0; # 0 disables blurring
      };

      input-field = {
        monitor = "";
        size = scaleArray [
          200
          50
        ];
        outline_thickness = osConfig.monitor_scale * 3;
        dots_size = 0.33; # Scale of input-field height, 0.2 - 0.8
        dots_spacing = 0.15; # Scale of dots' absolute size, 0.0 - 1.0
        dots_center = true;
        dots_rounding = -1; # -1 default circle, -2 follow input-field rounding
        outer_color = "rgb(${osConfig.nord3})";
        inner_color = "rgb(${osConfig.nord4})";
        font_color = "rgb(${osConfig.nord0})";
        fade_on_empty = true;
        fade_timeout = 1000; # Milliseconds before fade_on_empty is triggered.
        placeholder_text = "<i>the way is shut</i>"; # Text rendered in the input box when it's empty.
        hide_input = false;
        rounding = -1; # -1 means complete rounding (circle/oval)
        check_color = "rgb(${osConfig.nord12})";
        fail_color = "rgb(${osConfig.nord11})"; # if authentication failed, changes outer_color and fail message color
        fail_text = "<i>$FAIL <b>($ATTEMPTS)</b></i>"; # can be set to empty
        fail_transition = 300; # transition time in ms between normal outer_color and fail_color
        capslock_color = -1;
        numlock_color = -1;
        bothlock_color = -1; # when both locks are active. -1 means don't change outer color (same for above)
        invert_numlock = false; # change color if numlock is off
        swap_font_color = false; # see below
        position = scaleArray [
          0
          (builtins.fromJSON "-20")
        ];
        halign = "center";
        valign = "center";
      };

      label = [
        {
          monitor = "";
          #clock
          text = ''cmd[update:1000] echo "$TIME"'';
          color = "rgb(${osConfig.nord5})";
          font_size = osConfig.monitor_scale * 55;
          font_family = "DejaVuSansM Nerd Font";
          position = scaleArray [
            (builtins.fromJSON "-100")
            (builtins.fromJSON "60")
          ];
          halign = "right";
          valign = "bottom";
          shadow_passes = 5;
          shadow_size = osConfig.monitor_scale * 10;
        }
        # {
        #   monitor = "";
        #   text = "$USER";
        #   color = "rgb(${osConfig.nord5})";
        #   font_size = osConfig.monitor_scale * 20;
        #   font_family = "DejaVuSansM Nerd Font";
        #   position = scaleArray [ (builtins.fromJSON "-100") 160 ];
        #   halign = "right";
        #   valign = "bottom";
        #   shadow_passes = 5;
        #   shadow_size = osConfig.monitor_scale * 10;
        # }
      ];
    };
  };
}
